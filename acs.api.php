<?php

/*
 * @file
 * Hooks provided by the acs module.
 */

/**
 * Alter the options passed into the ACS Client.
 *
 * @param array $options
 *   A multideminsional array passed into the ACS Client.
 *   $options['options'] is the options passed into the SoapClient.
 */
function hook_acs_client_options_alter(&$options) {
  $options['options']['soap_version'] = SOAP_1_1;
}
