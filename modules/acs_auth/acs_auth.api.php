<?php

/**
 * @file
 * Hooks provided by the ACS Auth module.
 */

/**
 * Define the authentication options available for selection.
 *
 * @return
 *   An associative array of options and their meta data. This is used to build
 *   the authentication controller, generare the selection list for admin, and
 *   is passed into the constructor of the defined class.
 *   The top level key is the unique name of the option. The next level items are:
 *   - 'description': The user facing description for the admin form.
 *   - 'class': A class based controller system powers the authentication. This
 *     is the class name to be instanced.
 *   - 'cache': If the controller can be cached for the entirety of the page
 *     load. Unless there is a reason otherwise this should be TRUE.
 */
function hook_acs_auth_info() {
  return array(
    'example_name' => array(
      'description' => t('A user facing description of example_name option'),
      'class' => 'ExampleClass',
      'cache' => TRUE,
    ),
  );
}

/**
 * Change the options available for ACS Authentication.
 *
 * @param array $options
 *   A multimensional array of options.
 *   @see hook_acs_auth_info()
 */
function hook_acs_auth_info_alter(&$options) {
  $options['example_name']['class'] = 'OtherExampleClass';
}
