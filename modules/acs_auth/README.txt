
Summary
-------
The ACS Auth module provides the ability for users to login to Drupal with
ACS credentials. There are two login options provided by this module (though it
is extensible and other modules can provide other options). The two options are:
- Exclusive: With the exception of uid 1 all other users authenticate against ACS.
- Mixed: Local user authentication is attempted first, if no user is found auth
  against ACS is attempted.

For API integration details see API.txt.

Requirements
------------
- QueryPath: http://drupal.org/project/querypath

Installation
------------
See http://drupal.org/node/70151 for installation details.