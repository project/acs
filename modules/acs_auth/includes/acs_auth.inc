<?php

/**
 * Provide a base (abstract) class for other ACS Auth classes to use.
 */
abstract class ACSAuthBase {

  /**
   * When a response comes back from ACS there is a lot of useful data. That
   * data is stored here for later use.
   *
   * @var unknown_type
   */
  protected $data = NULL;

  /**
   * The info defined in hook_acs_auth_info().
   *
   * @var $info
   */
  protected $info = array();

  public function __construct($info) {
    $this->info = $info;
  }

  /**
   * Checks if the feature was enabled in the admin.
   *
   * @see ACSAuthInterface::isEnabled()
   */
  public function isEnabled() {
    return variable_get('acs_auth_enabled', FALSE);
  }

  /**
   * Check if UID 1. UID 1 is a special case in Drupal and is the root user.
   *
   * @param string $name
   *   The username attempting to login.
   *
   * @return
   *   TRUE if the username is UID 1 and FALSE otherwise.
   */
  public function isUid1($name = '') {
    // If the user is UID 1 process against Drupal. This needs to happen so the
    // root account is always able to login.
    $result = db_query("SELECT uid FROM {users} WHERE name = '%s' AND uid = '1'", $name);
    if ($account = db_fetch_object($result)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Authenticate a user against ACS.
   *
   * To simplify reading the XML from ACS we are using QueryPath here. It is
   * jQuery like but for PHP and XML/HTML.
   * @link http://querypath.org
   *
   * @param string $username
   *   The username to authenticate against.
   * @param string $password
   *   The corresponding password.
   *
   * @return
   *  TRUE if the user authenticated and FALSE if it failed.
   */
  public function authenticate($username, $password) {
    $return = FALSE;

    if (!empty($username)) {
      $client = acs_request('access');

      // filter_xss() is used to make sure well formed data is sent to ACS.
      // bHashCode is unknown. When we pass in 0 the call works. bHashCode is
      // not listed in the paramaters that need to be sent to ACS but is in the
      // the example xml. If it is not included the call will fail.
      $response = $client->validateAccessACSLogin(array('username' => filter_xss($username), 'password' => filter_xss($password), 'bHashCode' => 0));

      // What we have is a xml piece and not a full document. So, we parse it
      // with the xml() method instead of passing into the constructor.
      $xml = qp()->xml($response->validateAccessACSLoginResult->any);
      $username = $xml->find('UserName')->eq(0)->text();
      $disabled = $xml->parent()->find('disabled')->eq(0)->text();
      if (!empty($username) && $disabled == 'false') {
        $return = TRUE;

        // We collect information off the response to be used by other methods.
        $data = array();
        $items = $xml->parent()->children();
        foreach ($items as $k => $v) {
          $data[$v->tag()] = $v->text();
        }
        $this->data = $data;
      }
    }

    return $return;
  }

  /**
   * Create a new user from the information returned from ACS.
   *
   * @return stdClass
   *   A user object (already having been saved to the database).
   */
  public function createUser() {
    // Generate a random drupal password. ACS will be used for auth anyways.
    $pass_new = user_password(20);

    // If mail attribute is missing, set the name as mail.
    $mail = !empty($this->data['email']) ? $this->data['email'] : $this->data['UserName'];

    $userinfo = array('name' => $this->data['UserName'], 'pass' => $pass_new, 'mail' => $mail, 'init' => $mail, 'status' => 1, 'acs_auth' => TRUE);
    $account = user_save(NULL, $userinfo);
    watchdog('ACS', 'New ACS user %name was created.', array('%name' => $this->data['UserName']), WATCHDOG_NOTICE, l(t('edit'), 'user/'. $account->uid .'/edit'));

    return $account;
  }

  abstract public function validate(array $values = array());
}

/**
 * Interface for the ACS Auth Option.
 */
interface ACSAuthInterface {

  /**
   * When the class is created by acs_auth_controller() the info defined in
   * hook_acs_auth_info() for this option is passed into the constructor.
   *
   * @param array $info
   *   Information for the option described in hook_acs_auth_info().
   */
  public function __construct($info);

  /**
   * If the ACS Auth for this setup is properly configured and enabled. This is
   * useful if there is setup for this functionality that needs to be checked.
   *
   * @return bool
   *   TRUE if enable and FALSE if not.
   */
  public function isEnabled();

  /**
   * Authenticate a user.
   *
   * @param $values
   *   The authentication values passed in from different possible forms. For
   *   more detail see user_authenticate().
   */
  public function validate(array $values = array());
}