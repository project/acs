<?php

/**
 * With the exception of uid 1, all users are authenticated against ACS.
 */
class ACSAuthExclusive extends ACSAuthBase {

  /**
   * Authenticate all users, except uid 1, against ACS.
   *
   * @see ACSAuthInterface::validate()
   */
  public function validate(array $values = array()) {
    global $user;
    $name = $values['name'];
    $pass = trim($values['pass']);

    // If the user is UID 1 process against Drupal. This needs to happen so the
    // root account is always able to login.
    if ($this->isUid1($name)) {
      user_authenticate($values);
      return;
    }

    if (!$this->authenticate($name, $pass)) {
      return;
    }

    $account = user_load(array('name' => $name));

    if (!isset($account->uid)) {
      // Create the account.

      // Check if the username is allowed.
      if (drupal_is_denied('user', $name)) {
        drupal_set_message(t('The name %name has been denied access.', array('%name' => $name)), 'error');
        return;
      }

      // Create the new user from the ACS data.
      $account = $this->createUser();
    }
    elseif ($account->status == 0) {
      // Account diabled.
      return;
    }

    $user = $account;
    user_authenticate_finalize($values);
    return $user;
  }

}