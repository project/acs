<?php

/**
 * Users are first checked against the local install. If not there ACS
 * auth is attempted.
 */
class ACSAuthMixed extends ACSAuthBase {

  /**
   * Validate locally first and secondly against ACS.
   *
   * @see ACSAuthBase::validate()
   */
  function validate(array $values = array()) {
    global $user;
    $name = $values['name'];
    $pass = trim($values['pass']);

    // If the user is UID 1 process against Drupal. This needs to happen so the
    // root account is always able to login.
    if ($this->isUid1($name)) {
      user_authenticate($values);
      return;
    }

    // Authenticate local users before attempting ACS.
    if ($this->isLocalUser($name)) {
      user_authenticate($values);
      return;
    }

    // Not a local user so we check ACS.
    if (!$this->authenticate($name, $pass)) {
      return;
    }

    $account = user_load(array('name' => $name));

    if (!isset($account->uid)) {
      // Create the account.

      // Check if the username is allowed.
      if (drupal_is_denied('user', $name)) {
        drupal_set_message(t('The name %name has been denied access.', array('%name' => $name)), 'error');
        return;
      }

      // Create the new user from the ACS data.
      $account = $this->createUser();
    }
    elseif ($account->status == 0) {
      // Account diabled.
      return;
    }

    $user = $account;
    user_authenticate_finalize($values);
    return $user;
  }

  /**
   * Is the username a local user?
   *
   * @param string $name
   *   The username we are checking for.
   *
   * @return
   *   TRUE if the user is local and FALSE otherwise.
   */
  public function isLocalUser($name) {
    $result = db_query("SELECT name, data FROM {users} WHERE name='%s'", $name);
    if ($row = db_fetch_array($result)) {
      $data = unserialize($row['data']);
      if (!isset($data['acs_auth'])) {
        // The user is local.
        return TRUE;
      }
    }
    return FALSE;
  }
}
