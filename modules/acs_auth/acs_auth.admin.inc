<?php

/**
 * @file
 * Admin only components of acs_auth.
 */

/**
 * Settings form for ACS Authentication features.
 */
function acs_auth_settings_form() {
  $form = array();

  $form['acs_auth_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable ACS Authentication'),
    '#default_value' => variable_get('acs_auth_enabled', FALSE),
  );

  $option_list = acs_auth_info_list();
  $options = array();
  if (!empty($option_list)) {
    foreach ($option_list as $k => $v) {
      $options[$k] = $v['description'];
    }
  }

  $form['acs_auth_type'] = array(
    '#type' => 'radios',
    '#title' => t('Authentication type'),
    '#options' => $options,
    '#required' => TRUE,
    '#default_value' => variable_get('acs_auth_type', NULL),
  );

  $form['acs_auth_remove_pass_request'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove Request new password links and form'),
    '#description' => t('This is userful if users are not able to change their password on the site.'),
    '#default_value' => variable_get('acs_auth_remove_pass_request', FALSE),
  );

  $form['#submit'][] = 'acs_auth_admin_submit';

  return system_settings_form($form);
}

/**
 * When the remove pass request is selected the menu needs to be rebuilt.
 */
function acs_auth_admin_submit($form, &$form_state) {
  menu_rebuild();
}
