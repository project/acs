<?php

/**
 * @file
 * Admin settings forms for the ACS module.
 */

/**
 * Form callback; The admin settings form for the ACS module.
 */
function acs_api_settings_form() {
  $form = array();

  $form['acs_config'] = array(
    '#type' => 'fieldset',
  );
  $form['acs_config']['acs_site_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Number'),
    '#description' => t('Enter the ACS Technologies site number.'),
    '#default_value' => variable_get('acs_site_number', NULL),
    '#required' => TRUE,
  );

  // Field for the Security ID (secid)
  $form['acs_config']['acs_secid'] = array(
    '#type' => 'textfield',
    '#title' => t('Security ID'),
    '#description' => t('Enter the Security ID (secid) provided by ACS Technologies. If you do not already have API access please <a href="!url">contact ACS Technologies</a>.', array('!url' => 'http://www.acstechnologies.com/foundationalchurches/accessacs/api.htm')),
    '#default_value' => variable_get('acs_secid', NULL),
    '#required' => TRUE,
  );

  $form['acs_config']['acs_proxy'] = array(
    '#type' => 'fieldset',
    '#title' => t('Proxy Support'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('If the API requests need to pass through a proxy server please fill in the information below.'),
  );
  $form['acs_config']['acs_proxy']['acs_proxy_login'] = array(
    '#type' => 'textfield',
    '#title' => t('Login'),
    '#default_value' => variable_get('acs_proxy_login', NULL),
  );
  $form['acs_config']['acs_proxy']['acs_proxy_pass'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => variable_get('acs_proxy_pass', NULL),
    '#description' => t('Warning: the password is stored as plain text in the database.'),
  );
  $form['acs_config']['acs_proxy']['acs_proxy_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Host'),
    '#default_value' => variable_get('acs_proxy_host', NULL),
  );
  $form['acs_config']['acs_proxy']['acs_proxy_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Port'),
    '#default_value' => variable_get('acs_proxy_port', NULL),
  );

  $form['#validate'][] = 'acs_api_settings_form_validate';

  return system_settings_form($form);
}

function acs_api_settings_form_validate($form, &$form_state) {
  // Make sure we have the site number and secid. If they are not present core
  // will will throw an error.
  if (!empty($form_state['values']['acs_site_number']) && !empty($form_state['values']['acs_secid'])) {
    $options = array(
      'siteid' => $form_state['values']['acs_site_number'],
      'secid' => $form_state['values']['acs_secid'],
      // By passing a NULL token in one is not grabbed from the cache.
      'token' => NULL,
      'options' => array(
        'proxy_host' => $form_state['values']['acs_proxy_host'] ? $form_state['values']['acs_proxy_host'] : '',
        'proxy_login' => $form_state['values']['acs_proxy_login'] ? $form_state['values']['acs_proxy_login'] : '',
        'proxy_password' => $form_state['values']['acs_proxy_pass'] ? $form_state['values']['acs_proxy_pass'] : '',
      ),
    );
    // If an empty proxy port is passed in it is assigned a port of 0 and trys
    // to connect to a port of 0. We can only pass a port in if it is not empty.
    if (!empty($form_state['values']['acs_proxy_port'])) {
      $options['options'] = $form_state['values']['acs_proxy_pass'];
    }

    $client = acs_request('access', $options);
    $token = $client->getLoginToken();
    if (!isset($token) || !is_string($token)) {
      form_set_error('acs_config', t('The provided information does not provide access to the ACS API.'));
    }
  }
}
