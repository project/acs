<?php

/**
 * @file
 * An ACS Soap Client class for interfacing with the ACS SOAP Interface.
 */

/**
 * The ACS Client Class enables access to the SOAP API.
 *
 * The ACS SOAP API requires a token to be passed at each call. The token
 * expires after about an hour of time and then needs to be refreshed. This
 * class takes care of automating the token usage. Only when a token fails to
 * be refreshed is the token error thrown.
 */
class ACSClient extends SoapClient {

  /**
   * The token required by ACS on each call to the SOAP API. There are three
   * possible values for the token:
   * - A token as a string.
   * - NULL if there is no token.
   * - FALSE if a token refresh has failed.
   */
  protected $token = NULL;

  /**
   * The site id for the ACS site being accessed.
   */
  protected $siteid = NULL;

  /**
   * The secure id for the ACS site being accessed.
   */
  protected $secid = NULL;

  /**
   * The SoapClient controller object.
   */
  protected $controller = NULL;

  /**
   * Initialize the ACS Soap Client.
   *
   * @param string $endpoint
   *   The endpoint we are connecting to.
   * @param array $options
   *   - siteid: The site id for the ACS site being accessed.
   *   - secid: The secure id for the site being accessed.
   *   - token: An ACS Token. (optional)
   *   - options: The SoapClient options.
   *     @link http://php.net/SoapClient
   */
  public function __construct($endpoint, array $options = array()) {
    $this->siteid = $options['siteid'];
    $this->secid = $options['secid'];

    // Initialize a SoapClient object to make the api calls.
    $this->controller = new SoapClient($endpoint, $options['options']);

    // There are cases where we will pass in a NULL value as the token. For
    // example, when we validate the options form. On the options form we do
    // not want to get the cached result.
    if (array_key_exists('token', $options)) {
      $this->token = $options['token'];
    }
    // Check the cache for a copy of the token.
    elseif ($cached = cache_get('acs_token', 'cache')) {
      $this->token = $cached->data;
    }
  }

  /**
   * Make a call to the ACS SOAP API. The name is the operation being preformed
   * on the interface.
   *
   * The required token is automatically added to the API call. If the token is
   * stale it will be refreshed and the call tried again. If a refresh fails an
   * error will be thrown.
   *
   * @link https://secure.accessacs.com/acscfwsv2/wsca.asmx
   * @link https://secure.accessacs.com/acscfwsv2/wscea.asmx
   *
   * @param $operation
   *   The operation being called on the API.
   * @param $arguments
   *   Arguments to pass on to the API.
   *
   * @todo Switch from returning FALSE to throwing Exceptions on error.
   */
  public function __call($operation, array $arguments) {
    $params = (isset($arguments[0]) && is_array($arguments[0])) ? $arguments[0] : array();

    // The token value is null so we need to refresh the token.
    if (is_null($this->token)) {
      // Get the token
      $this->getLoginToken();
    }

    // We have a token, make a call to the api.
    if (!is_null($this->token) && $this->token != FALSE) {
      try {
        $params['token'] = $this->token;
        return $this->controller->{$operation}($params);
      }
      catch (SoapFault $e) {
        // Check for an expired token. We cannot check for an exact error or
        // error message as there seem to be variations on the same message.
        if (strpos($e->getMessage(), 'Unable to validate token. Token not valid.')) {
          // Remove the token from cache.
          cache_clear_all('acs_token', 'cache');
          $this->token = NULL;

          // Try using __soapCall again. This will try to refresh the token before
          // calling to the API.
          return $this->__call($operation, $arguments);
        }
        else {
          watchdog('ACS', 'The ACS API reported the following error: %error', array('%error' => $e->getMessage()), WATCHDOG_ERROR);
          return FALSE;
        }
      }
    }

    // Refreshing the token has already failed.
    return FALSE;
  }

  /**
   * Get the current API Token.
   *
   * @return
   *   The token or FALSE if unable to obtain a token.
   */
  public function getLoginToken() {
    // If no token has been grabbed, we go out and try to get one.
    if (is_null($this->token)) {
      try {
        // When there is no token we do not want to pass this through our
        // token handling SoapClient. Instead we request it with a SoapClient
        // based object and continue.
        $response = $this->controller->getLoginToken(array(
          'siteid' => $this->siteid,
          'secid' => $this->secid,
        ));

        $this->token = $response->getLoginTokenResult;

        // Cache the token for future requests. Tokens are valid for an hour
        // so we cache them for that long.
        cache_set('acs_token', $this->token, 'cache', time() + 3600);
      }
      catch (SoapFault $e) {
        // The ACS API reported an error. Since there was an error we set the
        // token to FALSE so other calls in the current page request stop.
        $this->token = FALSE;

        watchdog('ACS', 'The ACS API reported the following error: %error', array('%error' => $e->getMessage()), WATCHDOG_ERROR);
      }
    }
    return $this->token;
  }
}
