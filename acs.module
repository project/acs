<?php

/**
 * @file
 * Provide base integration with the ACS SOAP API for other modules to extend.
 */

/**
 * Retrieve ACS Client object to access the API with.
 *
 * This factory builds objects that can access the ACS API. The ACS API uses
 * SOAP to access and requires a token. The ACS Client object automates usage
 * of the required token including refreshing the token when it expires.
 *
 * The factory has default classes which are used. These can be overriden either
 * with alternatice classes being passed in via the $options or by altering the
 * default classes in the Drupal variables of acs_class and acs_soap_class.
 * @see variable_set()
 *
 * For the available operation on the ACS API please refer to the ACS API
 * documentation.
 * @link https://secure.accessacs.com/acscfwsv2/wsca.asmx
 * @link https://secure.accessacs.com/acscfwsv2/wscea.asmx
 *
 * @param string $type
 *   Type of API reference.
 *   @see acs_get_link
 * @param array $options
 *   An array of options. The available options are:
 *     - acs_class: The ACS Client class to return. Defaults to ACSClient.
 *       This can be overridden by altering the Drupal variable for acs_class.
 *     - options: An array of options to pass into the Soap Client. This
 *       is useful for cases when a proxy is used. For more detail see the
 *       $options argument on the SoapClient. Defaults to configured value.
 *       @link http://us2.php.net/manual/en/soapclient.soapclient.php
 *     - secid: The SecID for the ACS site. Defaults to configured value.
 *     - siteid: The site number for the ACS site. Defaults to
 *       configured value.
 * @param bool $reset
 *   Set to TRUE to clear the cache of the object for the specified type.
 *
 * @return
 *   SoapClient Object setup for the ACS configuration.
 *   FALSE: if there was a problem building the object.
 */
function acs_request($type = NULL, array $options = array(), $reset = FALSE) {
  static $request_objects = array();

  $options += array(
    'siteid' => variable_get('acs_site_number', NULL),
    'secid' => variable_get('acs_secid', NULL),
    'cache' => TRUE,
  );

  // Clear the static cache of ACS Objects.
  if ($reset) {
    $request_objects = array();
  }

  // Return a cached object.
  if (isset($request_objects[$type]) && $options['cache']) {
    return $request_objects[$type];
  }

  if ($endpoint = acs_get_endpoint($type)) {
    $acs_class = (!empty($options['acs_class'])) ? $options['acs_class'] : variable_get('acs_class', 'ACSClient');

    // Add default SOAP Options.
    if (!isset($options['options']) || !is_array($options['options'])) {
      $options['options'] = array();
    }
    $options['options'] += array('soap_version' => SOAP_1_2);

    // The proxy setting are only added if they were not passed in to the factory.
    if (variable_get('acs_proxy_host', NULL)) {
      $options['options'] += array('proxy_host' => variable_get('acs_proxy_host', NULL));
      if (variable_get('acs_proxy_login', NULL)) {
        $options['options'] += array('proxy_login' => variable_get('acs_proxy_login', NULL));
      }
      if (variable_get('acs_proxy_pass', NULL)) {
        $options['options'] += array('proxy_password' => variable_get('acs_proxy_pass', NULL));
      }
      if (variable_get('acs_proxy_port', NULL)) {
        $options['options'] += array('proxy_port' => variable_get('acs_proxy_port', NULL));
      }
    }

    // Offer an opportunity for other modules to alter the options.
    drupal_alter('acs_client_options', $options);

    $client = new $acs_class($endpoint, $options);
    if ($options['cache']) {
      $request_objects[$type] = $client;
    }

    // @todo Add error reporting if objects not properly created.
    return $client;
  }

  // No valied endpoint was chosen.
  watchdog('ACS', 'Invalid ACS Endpoint requested.', WATCHDOG_ERROR);
  return FALSE;
}

 /**
 * Implementation of hook_menu().
 */
function acs_menu() {
  $items = array();
  $items['admin/settings/acs'] = array(
    'title' => 'ACS',
    'description' => 'ACS Integration',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('acs_api_settings_form'),
    'access arguments' => array('administer acs configuration'),
    'file' => 'acs.admin.inc',
  );
  $items['admin/settings/acs/api'] = array(
    'title' => 'API Settings',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  return $items;
}

/**
 * Implementation of hook_perm().
 */
function acs_perm() {
  return array('administer acs configuration');
}

/**
 * Get the link to the SOAP API for the request.
 *
 * Development Notes: New links are added here. Or, should they be handled
 * with a registry hook? Either way, this provides one place to get the needed
 * one.
 *
 * @param string $type
 *   The API reference. Available options are:
 *     access: The access api. Details are available at
 *             https://secure.accessacs.com/acscfwsv2/wsca.asmx
 *     event: The event api. Details are available at
 *            https://secure.accessacs.com/acscfwsv2/wscea.asmx
 *
 * @return
 *   The reference URL.
 *   FALSE if there was an error.
 */
function acs_get_endpoint($type = 'access') {
  $resources = array(
    'access' => 'https://secure.accessacs.com/acscfwsv2/wsca.asmx?WSDL',
    'event' => 'https://secure.accessacs.com/acscfwsv2/wscea.asmx?WSDL',
  );
  if (array_key_exists($type, $resources)) {
    return $resources[$type];
  }

  // Oops. Invalid input.
  return FALSE;
}

/**
 * Implementation of hook_autoload_info().
 */
function acs_autoload_info() {
  return array(
    'ACSClient' => array(
      'file' => 'acs.acsclient.inc',
    ),
  );
}
