
Summary
-------
The ACS module provides API integration with the ACS Technologies services. The
ACS module does not provide any end user features but instead provides the
foundation for other modules to build upon.

For API integration details see API.txt.

Requirements
------------
- SoapClient. This is included by default with PHP 5.2 and new.
- API access to an ACS Access account. To request access fill out the form at
  http://www.acstechnologies.com/foundationalchurches/accessacs/api.htm

Installation
------------
See http://drupal.org/node/70151 for installation details.